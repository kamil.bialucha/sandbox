import axios from 'axios';

export default class MatchService {
    constructor(apiUrl) {
        this.apiUrl = apiUrl;
    }

    async getMatches() {
        return await axios.get(`${this.apiUrl}/matches`);
    }

    async addMatch(match) {
        //TODO: pretty awful way you must pass object to json-server, instead of just passing match variable
        return await axios.post(`${this.apiUrl}/matches`, {
            name: match.name,
            address: match.address,
            totalCost: match.totalCost
        });
    }
}
