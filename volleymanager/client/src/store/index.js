import { createStore } from 'vuex';
import MatchService from '../services/matchService';

const store = createStore({
    state: {
        matches: []
    },
    getters: {
        someMatches: state => state.matches
    },
    mutations: {
        setMatches: (state, matches) => (state.matches = matches),
        appendMatch: (state, match) => state.matches.unshift(match)
    },
    actions: {
        async fetchMatches({ commit }) {
            //TODO: proper dependency injection
            const service = new MatchService(process.env.VUE_APP_API_URL);
            const response = await service.getMatches();

            commit('setMatches', response.data);
        },
        async addMatch({ commit }, payload) {
            //TODO: proper dependency injection
            const service = new MatchService(process.env.VUE_APP_API_URL);
            const response = await service.addMatch(payload);

            commit('appendMatch', response.data);
        }
    },
    modules: {}
});

export default store;
