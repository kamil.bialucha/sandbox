import React from 'react';
import { Link } from 'react-router-dom';
import './Header.scss';

export default function Header() {
    return (
        <div className="nav-container">
            <nav className="navigation">
                <ul className="links-list">
                    <li>
                        <Link to="/">Home</Link>
                    </li>
                    <li>
                        <Link to="/matches">Matches</Link>
                    </li>
                </ul>
            </nav>
        </div>
    );
}
