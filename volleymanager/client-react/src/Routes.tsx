import React from 'react';
import { Route } from 'react-router-dom';
import Home from './views/Home';
import Matches from './views/Matches';

export default function Routes() {
    return (
        <div className="view-content">
            <Route exact path="/" component={Home} />
            <Route path="/matches" component={Matches} />
        </div>
    );
}
