import React from 'react';
import './App.scss';
import Header from './components/navigation/Header';
import Footer from './components/navigation/Footer';
import Routes from './Routes';
import { BrowserRouter as Router } from 'react-router-dom';

function App() {
    return (
        <div>
            <Router>
                <Header />
                <Routes />
            </Router>

            <Footer></Footer>
        </div>
    );
}

export default App;
