import { Match } from "../models/match";
import DummyData from '../dummyDataClass';

export default class MatchService{
    matches: Match[];

    constructor(){
        const toCopy = new DummyData();
        this.matches = toCopy.data();
    }

    //TODO: parse data from .json file from json array to json objects array
    getMatches(){
        return this.matches;
    }
}