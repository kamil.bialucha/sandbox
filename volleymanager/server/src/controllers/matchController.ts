//TODO: refactor to be matchController class with private route field

import express, { Request, Response, Router } from 'express';
import MatchService from './../services/matchService';

let router: Router = express.Router();

router
    .route('/')
    .get((req: Request, res: Response) => {
        //TODO: refactor to use dependency injection
        const service: MatchService = new MatchService();
        service.getMatches();
        res.send(service.getMatches());
    })
    .post((req, res) => {
        console.log('not implemented.');
    });

module.exports = router;
