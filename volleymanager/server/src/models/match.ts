//TODO: refactor not to use constructor. parse JSON objects to TS objects

export class Match {
    id: number;
    name: string;
    address: string;
    time: Date;
    totalCost: number;

    constructor(id: number, name: string, address: string, time: Date, totalCost: number) {
        this.id = id;
        this.address = address;
        this.time = time;
        this.totalCost = totalCost;
        this.name = name;
    }
}
