import express, { Request, Response, Application } from 'express';
const matchController = require('./controllers/matchController');

const cors = require('cors');
const app: Application = express();

app.use(cors());
app.use('/matches', matchController);

app.get('/', (req, res) => {
    res.send('hello root');
});

app.listen(5000, () => {
    console.log('Server running...');
});
//abc